package ru.vartanyan.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractListener;
import ru.vartanyan.tm.util.NumberUtil;

@Component
public class InfoListener extends AbstractListener {

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public @NotNull String name() {
        return "show-info";
    }

    @Override
    public String description() {
        return "Show info";
    }

    @Override
    @EventListener(condition = "@infoListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[SYSTEM INFO]");

        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory (bytes): " + NumberUtil.format(freeMemory));

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final long maxMemoryValue = isMaxMemory ? Long.parseLong("no limit") : maxMemory;
        System.out.println("Maximum memory (bytes): " + NumberUtil.format(maxMemoryValue));

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM (bytes): " + NumberUtil.format(totalMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));

        System.out.println("[OK]");
    }

}
