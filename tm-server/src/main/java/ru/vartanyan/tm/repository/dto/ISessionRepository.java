package ru.vartanyan.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.IRepository;
import ru.vartanyan.tm.dto.Project;
import ru.vartanyan.tm.dto.Session;

import javax.persistence.EntityManager;
import java.util.List;

public interface ISessionRepository extends IRepository<Session>  {

    @Nullable
    Session findOneById(@Nullable final String id);

}

